﻿using System;
using System.Collections.Generic;

namespace BookManage.Models
{
    public partial class Book
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Price { get; set; }
    }
}
