﻿using BookManage.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookManage.Controllers
{
    [Route("api/books")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private DbContext _context;
        public BookController(DbContext context)
        {
            _context = context;
        }

         [HttpPost]
         public IActionResult Create([FromBody] CreateViewModel model)
        {
            try
            {
                var book = new Book
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = model.Name,
                    Description = model.Description,
                    Price = model.Price
                };
                var result = _context.Set<Book>().Add(book);
                if(result != null)
                {
                    _context.SaveChanges();
                    return Ok(result.Entity.Id);
                }
                else
                {
                    return BadRequest();
                }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {

                var result = _context.Set<Book>();
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("get-by-id")]
        public IActionResult GetById(string id)
        {
            try
            {

                var result = _context.Set<Book>().FirstOrDefault(s => s.Id == id);
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(string id)
        {
            try
            {

                var result = _context.Set<Book>().FirstOrDefault(s => s.Id == id);
                if (result != null)
                {
                    _context.Set<Book>().Remove(result);
                    _context.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut]
        public IActionResult Edit(string id, [FromBody] CreateViewModel model)
        {
            try
            {

                var result = _context.Set<Book>().FirstOrDefault(s => s.Id == id);
                if (result != null)
                {
                    result.Name = model.Name != null ? model.Name : result.Name;
                    result.Description = model.Description != null ? model.Description : result.Description;
                    result.Price = model.Price != 0 ? model.Price : result.Price;
                    _context.Set<Book>().Update(result);
                    _context.SaveChanges();
                    return Ok(result.Id);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }


    public class CreateViewModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("price")]
        public int Price { get; set; }
    }
}
